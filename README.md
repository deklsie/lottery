## 声明

源码只供学习使用，如用于商业活动与本人无关，请勿将系统用于非法业务

## 截图

| 投注 | 投注订单详情 | 追号 |
| :------: | :------: | :------: |
| ![投注](https://images.gitee.com/uploads/images/2019/0514/102402_3019ab96_527945.png) | ![投注订单详情](https://images.gitee.com/uploads/images/2019/0514/102327_943254ff_527945.png) | ![追号](https://images.gitee.com/uploads/images/2019/0514/102402_cdeb1172_527945.png) |

| 投注记录 | 充值 | 提现 |
| :------: | :------: | :------: |
| ![投注记录](https://images.gitee.com/uploads/images/2019/0514/102402_a5092fea_527945.png) | ![充值](https://images.gitee.com/uploads/images/2019/0514/102402_ba97d7e8_527945.png) | ![提现](https://images.gitee.com/uploads/images/2019/0514/102402_38088ddc_527945.png) |

> 由于程序不断优化，界面细节可能有所变化，请以实际页面为准

> 作者不喝咖啡 :coffee: 只喝茶 :tea: 觉得有帮助的可以请我喝茶


| 支付宝 | 微信支付 |
| :------: | :------: |
| ![alipay](https://images.gitee.com/uploads/images/2019/0514/102402_99982ec5_527945.jpeg) | ![wepay](https://images.gitee.com/uploads/images/2019/0514/102403_8cbd56a1_527945.png) |

